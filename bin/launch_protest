#!/usr/bin/env bash

set -eo pipefail

usage () {
    # Print usage information
    echo ""
    echo "Usage is ./launch_protest <test_type> <path to cheetah build tree>"
    echo "<test_type> options are:"
    echo "    'product'"
    echo ""
}

arg_check () {
    if [ -z "$*" ] ; then
        echo "No test type supplied"
        usage && exit 1
    elif [ "$TEST_TYPE" == "-h" ] ; then
        usage && exit 0
    fi

   case "$TEST_TYPE" in 
        product) 
            echo "TEST TYPE: $TEST_TYPE is valid" ;;
        *)
            usage && exit 1 ;;
    esac
}

fail () {
    echo ""
    echo "Something went wrong during the virtual environment installation."
    echo "Please check the error and try again."
    echo ""
    exit 1
}

# Receive arguments from user and check validity
TEST_TYPE=$1
EXE_PATH=$2
arg_check "$TEST_TYPE"

# Do we have python3?
python3 --version > /dev/null || {
    echo "python3 not available"
    echo "Please install it and retry"
    exit 1
}

# (Create and) start ProTest environment
if ! [ -f ~/.venvs/protest/bin/activate ] ; then
    echo "Creating virtual env"
    python3 -m venv ~/.venvs/protest || fail

    echo "Starting virtual env"
    source ~/.venvs/protest/bin/activate || fail

    echo "Upgrading pip"
    pip install --upgrade pip || fail

    echo "Installing ProTest"
    pip install --index-url https://artefact.skao.int/repository/pypi-internal/simple --extra-index-url https://pypi.org/simple ska-pss-protest --no-cache-dir || fail

    echo "Checking ProTest installation"
    protest -h > /dev/null 2>&1  || {
        echo "ProTest not installed"
        exit 1
    }

    echo "ProTest successfully installed"
else
    echo "Starting virtual env for ProTest"
    source ~/.venvs/protest/bin/activate || {
        echo "Couldn't start ProTest environment"
        exit 1
    }
fi

echo "Launching tests....."
protest -m ${TEST_TYPE} -p ${EXE_PATH}

# Tear down test environment
echo "Tests completed - deactivating PROTEST"
deactivate
