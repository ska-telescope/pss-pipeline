# ----------------------------------------------------
# Copyright The SKA Organisation 2022
# ----------------------------------------------------

# ----------------------------------------------------
# Function: generate a custom target to publish to the
#           SKAO Common Artifact Repoitory (CAR)
#           You should probably use ska_car_publish
#           rather than this function.
#
# Arguments:
#    target_name : name of the target to generate
#    product     : the file to publish
#    repo        : the name of the gitlab_ci variable that defines the target repositry on CAR
#    credentiols : a string in the firmat username:password to allow access to the repo
#
# ----------------------------------------------------
function(skao_car_publish_targets target_name product repo credentials)
    if( $ENV{${repo}} )
        add_custom_target(${target_name}
                      DEPENDS ${product}
                      COMMENT "SKAO CAR: uploading ${product}"
                      COMMAND "curl -v -u ${credentials} --upload-file ${product} $ENV{${repo}}"
              )
    else()
        add_custom_target(${target_name}
                          COMMAND ${CMAKE_COMMAND} -E echo
                          COMMENT "SKAO CAR: publish target only defined in a gitlab ci environment" 
                         )
    endif()
endfunction(skao_car_publish_targets)

# ----------------------------------------------------
# Function: generate a custom target to publish to the
#           SKAO Common Artifact Repoitory (CAR)
#
# Arguments:
#    target_name : name of the target to generate
#    product     : the file to publish
#
# Details:
#    This function will use the file extension to determine
#    The type of file and the associated CAR destinations
#    for this type. If the type is not supporte by CAR
#    a fatal error message will be issued    
# ----------------------------------------------------
function(skao_car_publish target_name product)
    if( NOT product )
        message(WARNING "no product file defined")
        return()
    endif()

    get_filename_component( pkg_type "${product}" EXT )
    if( "${pkg_type}" STREQUAL ".deb" )
        skao_car_publish_targets(${target_name} ${product} CAR_DEBIAN_REPOSITORY_URL "$ENV{CAR_DEBIAN_USERNAME}:$ENV{CAR_DEBIAN_PASSWORD}")
    elseif( "${pkg_type}" STREQUAL ".rpm" )
        skao_car_publish_targets(${target_name} ${product} CAR_RPM_REPOSITORY_URL "$ENV{CAR_RPM_USERNAME}:$ENV{CAR_RPM_PASSWORD}")
    else()
        message(FATAL_ERROR "SKAO CAR: Unsupported file type \"${product}\"")
    endif()
endfunction(skao_car_publish)

# ----------------------------------------------------
# Function: generate a SKAO CAR compliant manifest file
#           and add an install target for the file
# Arguments:
#   manifest_filename : the name of the file to generate
# ----------------------------------------------------
function(skao_car_manifest_file manifest_filename)

    # the set of gitlab CI variables to extract into the manifest
    set(ska_manifest_vars
      CI_COMMIT_AUTHOR
      CI_COMMIT_REF_NAME
      CI_COMMIT_REF_SLUG
      CI_COMMIT_SHA
      CI_COMMIT_SHORT_SHA
      CI_COMMIT_TIMESTAMP
      CI_JOB_ID
      CI_JOB_URL
      CI_PIPELINE_ID
      CI_PIPELINE_IID
      CI_PIPELINE_URL
      CI_PROJECT_ID
      CI_PROJECT_PATH_SLUG
      CI_PROJECT_URL
      CI_RUNNER_ID
      CI_RUNNER_REVISION
      CI_RUNNER_TAGS
      GITLAB_USER_NAME
      GITLAB_USER_EMAIL
      GITLAB_USER_LOGIN
      GITLAB_USER_ID
    )

    set(manifest_values "")

    foreach(man_var ${ska_manifest_vars})
      if(DEFINED ENV{${man_var}})
        list(APPEND manifest_values ${man_var}=$ENV{${man_var}})
      endif()
    endforeach()

    foreach(man_val ${manifest_values})
      file(APPEND ${manifest_filename} ${man_val})
    endforeach()

    if(EXISTS ${manifest_filename})
        install(FILES "${manifest_filename}"
            DESTINATION ${MODULES_INSTALL_DIR})
    endif()

endfunction(skao_car_manifest_file)

# -- generate a manifest file for the car
skao_car_manifest_file("${CMAKE_CURRENT_BINARY_DIR}/MANIFEST.skao.int")
