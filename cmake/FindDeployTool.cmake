# Find the deploytool and its requirements
# Variables:
#   DEPLOYTOOL_EXE   : the command to execute deploytool
#   DEPLOYTOOL_FOUND : set to TRUE if deploytool is found
#
# Functions:
#   deploytool_spin(result) : see doc below

include(FindPackageHandleCompat)

find_package(PythonInterp)
if(${PYTHONINTERP_FOUND})
    if(PYTHON_EXECUTABLE)
        # - workaround for different cmake versions
        set(Python_EXECUTABLE ${PYTHON_EXECUTABLE})
    endif()
    set(DEPLOYTOOL_EXE
        "${Python_EXECUTABLE}" "${PROJECT_SOURCE_DIR}/tools/deploytool"
       )
endif()

find_package_handle_standard_args(DeployTool REQUIRED_VARS DEPLOYTOOL_EXE HANDLE_COMPONENTS)
set(DEPLOYTOOL_FOUND ${DeployTool_FOUND})
mark_as_advanced(DEPLOYTOOL_EXE, DEPLOYTOOL_FOUND)


# -----------------------------------------------------------
# Examine the current ENABLE_ variables and define a matching
# spin specification suitable for use with the deploytool
# 
# Parameters
#   result : The variable to return the deploytool specification
#
# -----------------------------------------------------------
function(deploytool_spin result)
    get_cmake_property(vars VARIABLES)
    string(REGEX MATCHALL "(^|;)ENABLE_[A-Za-z0-9_]*" enable_vars "${vars}")
    foreach(dep ${enable_vars})
        if(${dep})
            string(REPLACE "ENABLE_" "" dep "${dep}")
            string(TOLOWER "${dep}" lower_dep)
            set(spin "${spin},${lower_dep}")
        endif()
    endforeach()
    if(spin)
        string(REGEX REPLACE "^," "" spin ${spin})
    else()
        set(spin "default")
    endif()
    set(${result} ${spin} PARENT_SCOPE)
endfunction(deploytool_spin)
