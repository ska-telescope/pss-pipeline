# Find the PSS product test launcher (protest)
# Variables:
#   PROTEST_EXE   : the command to execute protest
#   PROTEST_FOUND : set to TRUE if protest is found

include(FindPackageHandleCompat)

set(PROTEST_DIR ${PROJECT_SOURCE_DIR}/tests)

find_program(PROTEST_EXE launch_protest
             PATHS ${PROJECT_SOURCE_DIR}/bin)

find_package_handle_standard_args(Protest REQUIRED_VARS PROTEST_EXE HANDLE_COMPONENTS)
set(PROTEST_FOUND ${Protest_FOUND})
mark_as_advanced(PROTEST_EXE, PROTEST_FOUND)

add_custom_target(test_product
    COMMAND ${PROTEST_EXE} product ${CHEETAH_BUILD_DIR}/cheetah
    )
