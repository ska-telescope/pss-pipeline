# -----------------------------------------------------------------------------------
# PACKAGING
# ==========
# Generates system installable packages suitable for the current build machine
# Includes dependecy information for the currently configured spin
#
# Controlling Variables
# ---------------------
# PACKAGE_NAME          : the name of the pacxakge (defaults to PROJECT_NAME)
# PACKAGE_VERSION       : a string containing a sementic version specification for the package (i.e. major.minor.patch)
# CPACK_PACKAGE_CONTACT : defaults to PROJECT_CONTACT_EMAIL
# PACKAGE_LICENSE_FILE  : Set a sepecifc license file for the pacakage
#
# Variables Set
# -------------
# PACKAGE_FILE : the name of the generated pacakge file
# -----------------------------------------------------------------------------------

find_package(DeployTool)

if(${DEPLOYTOOL_FOUND})

    set(cpack_dependency_file "${CMAKE_BINARY_DIR}/cpack_gen.cmake")
    deploytool_spin(DEPLOYTOOL_SPIN)
    message("package generation for build spin: ${DEPLOYTOOL_SPIN}")

    # use the deploytool to generate the cpack settings for this product
    file(WRITE "${cpack_dependency_file}" "# Generated file\n")
    file(APPEND "${cpack_dependency_file}" "# ${DEPLOYTOOL_EXE} ${DEPLOYTOOL_OPTIONS} -s ${DEPLOYTOOL_SPIN} cpack\n")
    execute_process(
        COMMAND ${DEPLOYTOOL_EXE} ${DEPLOYTOOL_OPTIONS} -s ${DEPLOYTOOL_SPIN} cpack
        OUTPUT_VARIABLE exec_output
        ERROR_VARIABLE exec_output
        RESULT_VARIABLE exec_result
    )
    if( ${exec_result} GREATER 0 )
        message(WARNING "deploytool error: unable to generate package dependency info")
        file(APPEND "${cpack_dependency_file}" ${exec_output})
    else()
        file(APPEND "${cpack_dependency_file}" ${exec_output})
        configure_file(${cpack_dependency_file} ${cpack_dependency_file} COPYONLY)
        include("${cpack_dependency_file}")
    endif()

endif(${DEPLOYTOOL_FOUND})

# -- Cpack specific setttings
if(NOT PACKAGE_VERSION)
    set(PACKAGE_VERSION ${PROJECT_VERSION})
endif()
include(${PROJECT_SOURCE_DIR}/thirdparty/cheetah/cmake/semantic_version.cmake)
extract_semantic_version(PACKAGE)
set(CPACK_PACKAGE_VERSION_MAJOR ${PACKAGE_VERSION_MAJOR})
set(CPACK_PACKAGE_VERSION_MINOR ${PACKAGE_VERSION_MINOR})
set(CPACK_PACKAGE_VERSION_PATCH ${PACKAGE_VERSION_PATCH})

if(NOT PACKAGE_NAME)
    set(PACKAGE_NAME ${PROJECT_NAME})
endif()
set(CPACK_PACKAGE_NAME ${PACKAGE_NAME})

# -- install version in here
if(NOT CPACK_PACKAGE_CONTACT)
    set(CPACK_PACKAGE_CONTACT ${PROJECT_CONTACT_EMAIL})
endif()

# licence file
if(EXISTS "${PACKAGE_LICENSE_FILE}")
    set(CPACK_RESOURCE_FILE_LICENSE "${PACKAGE_LICENSE_FILE}")
endif()

set(CPACK_PACKAGING_INSTALL_PREFIX "/opt/${PACKAGE_NAME}/${PACKAGE_VERSION}")

include(CPack)
