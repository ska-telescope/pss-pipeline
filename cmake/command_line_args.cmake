#
# function to extract command line arguments into the passed CL_ARGS that were passed to cmake
# Must be called before project()
#
macro(command_line_args VARNAME)
    get_cmake_property(vars CACHE_VARIABLES)
    foreach(var ${vars})
        get_property(help_string CACHE "${var}" PROPERTY HELPSTRING)
        if("${help_string}" MATCHES "No help, variable specified on the command line." OR "${help_string}" STREQUAL "")
            # message("${var} = [${${var}}]  --  ${help_string}") # uncomment to see the variables being processed
            list(APPEND ${VARNAME} "-D${var}=${${var}}")
        endif()
    endforeach()
endmacro(command_line_args)
