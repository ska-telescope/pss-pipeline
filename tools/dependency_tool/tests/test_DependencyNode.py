""" Unit tests for the DependencyNode class"""
from dependencytool.DependencyNode import DependencyNode
from dependencytool.Dependency import Dependency
from dependencytool.Version import Version

def test_find_with_attributes():
    node = DependencyNode(None, Dependency("RA", Version("1.0"), build=True))

    node.add_dependency(Dependency("A", Version("1.0"), build=True))
    results_A = node.find_with_attribute( { "build": True } )
    assert len(results_A) == 1

    results_B = node.find_with_attribute( { "build": False } )
    assert len(results_B) == 0

    results_C = node.find_with_attribute( { "optional": True } )
    assert len(results_C) == 0

def test_find_first_by_name_no_subdependencies():
    node = DependencyNode(None, Dependency("ra", Version("1.0")))
    assert node.find_first("ra") == node
    assert node.find_first("a") is None

def test_find_first_by_name_with_subdependencies():
    node = DependencyNode(None, Dependency("ra", Version("1.0")))
    node.add_dependency(Dependency("a", Version("1.0"), build=True))
    assert node.find_first("b") is None
    assert node.find_first("a") == node.subdependencies()[0]

def test_import_deps_from_root_node():
    base_node = DependencyNode(None)
    node1_a = DependencyNode(None, Dependency("l1_a", Version("1.0")))
    base_node.import_deps(node1_a)
    assert base_node.find_first("b") is None

    result = base_node.find_first("l1_a")
    assert result is not None
    assert result.name() == node1_a.name()
    assert result == base_node.subdependencies()[0]
    assert result.parent() == base_node

def test_import_deps_optional_sub_node():
    # create a dependecy tree to overwrite
    base_node = DependencyNode(None, Dependency("ra", Version("1.0")))
    sub_node = base_node.add_dependency(Dependency("l1_a", Version("1.0")))
    sub_node.add_dependency(Dependency("l2_a", Version("1.0"))) # to overwrite
    sub_node.add_dependency(Dependency("l2_b", Version("1.5"))) # to remain unchanged
    attr_results_pre = base_node.find_with_attribute( { "optional": True } )
    assert len(attr_results_pre) == 0

    # construct a DependecyNode to import which overwrites some of the
    # original tree
    node_a = DependencyNode(None, Dependency("l1_a", Version("2.0")))
    node_a.add_dependency( Dependency("l2_a", Version("2.0"), optional=True) )

    # perfrom the import_deps
    base_node.import_deps(node_a)

    # check we have the node_a tree in place
    assert len(base_node.subdependencies()) == 1
    assert base_node.subdependencies()[0].parent() == base_node
    assert base_node.find_first("l1_a").version() == node_a.version()

    # expect version in subtree of package to be replaced
    l2a = base_node.find_first("l2_a")
    assert l2a.version() == Version("2.0")
    assert l2a.optional()
    assert l2a.parent() == base_node.subdependencies()[0]

    # expect version in subtree that are not replaced to be left alone
    l2b = base_node.find_first("l2_b")
    assert l2b.version() == Version("1.5")
    assert not l2b.optional()
    assert l2b.parent() == base_node.subdependencies()[0]

    # verify attributes are propogated
    attr_results_post = base_node.find_with_attribute_recursive( { "optional": True } )
    assert len(attr_results_post) == 1
