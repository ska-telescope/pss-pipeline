import os.path
import pytest
from dependencytool.DependencyFile import DependencyFile
from dependencytool.Version import Version

def data_file(filename):
    dirname, basename = os.path.split(__file__)
    stem = os.path.splitext(basename)[0]
    return os.path.join(dirname, stem + "_files/" , filename)


def test_bad_filename():
    with pytest.raises(TypeError):
        dep_file = DependencyFile(123)

def test_empty_filename():
    with pytest.raises(FileNotFoundError):
        dep_file = DependencyFile("")

def test_empty_list():
    filename = data_file("empty_list")
    dep_file = DependencyFile(filename)
    assert len(dep_file.dependencies()) == 0

def test_simple_list():
    filename = data_file("simple_list")
    dep_file = DependencyFile(filename)
    root_deps = dep_file.dependencies()
    assert len(root_deps) == 3
    assert root_deps[0].name() == "a"
    assert root_deps[0].version() == Version("1.0")
    assert root_deps[1].name() == "b"
    assert root_deps[1].version() == Version("2.0")
    assert root_deps[2].name() == "c"
    assert root_deps[2].version() == Version("3.0")

def test_single_optional_dependency():
    filename = data_file("single_optional_dependency")
    dep_file = DependencyFile(filename)
    root_deps = dep_file.dependencies()
    assert len(root_deps) == 1
    assert len(root_deps[0].subdependencies()) == 1
    assert root_deps[0].name() == "a"
    assert root_deps[0].version() == Version("1.0")
    a_deps = root_deps[0].subdependencies()
    assert len(a_deps) == 1
    assert a_deps[0].name() == "a_b"
    assert a_deps[0].version() == Version("4.0")
    assert root_deps[0].optional() == True
    assert a_deps[0].optional() == False

def test_multiple_optional_subdependency_last():
    filename = data_file("multiple_optional_dependency_last")
    dep_file = DependencyFile(filename)
    root_deps = dep_file.dependencies()
    assert len(root_deps) == 2
    assert root_deps[0].name() == "a"
    assert root_deps[0].version() == Version("1.0")
    a_deps = root_deps[0].subdependencies()
    assert len(a_deps) == 1
    assert a_deps[0].name() == "a_b"
    assert a_deps[0].version() == Version("4.0")
    assert root_deps[0].optional() == True

    b_deps = root_deps[1].subdependencies()
    assert root_deps[1].name() == "b"
    assert root_deps[1].version() == Version("99.0.4")
    assert root_deps[1].optional() == False
    assert len(b_deps) == 2
    assert b_deps[0].name() == "b_a"
    assert b_deps[1].name() == "b_b"

def test_multiple_subdependency_layers():
    filename = data_file("multiple_subdependency_layers")
    dep_file = DependencyFile(filename)
    root_deps = dep_file.dependencies()
    assert len(root_deps) == 3

    assert root_deps[0].name() == "a"
    assert root_deps[0].version() == Version("1.0")
    assert len(root_deps[0].subdependencies()) == 0

    assert root_deps[1].name() == "b"
    assert root_deps[1].version() == Version("99.0.4")
    b_deps = root_deps[1].subdependencies()
    assert len(b_deps) == 2
    b_a_deps = b_deps[0].subdependencies()
    assert len(b_a_deps) == 1
    b_b_deps = b_deps[1].subdependencies()
    assert len(b_b_deps) == 1

    assert root_deps[2].name() == "c"
    assert root_deps[2].version() == Version("22.0")
    assert len(root_deps[2].subdependencies()) == 0
