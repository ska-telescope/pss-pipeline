import subprocess

class Command(object):
    """ A command to be executed on the command line
    """
    def __init__(self, command_line, name=""):
        self._name = name
        self._ln = command_line

    def name(self):
        return self._name

    def __str__(self):
        return self._ln

    def __eq__(self, other):
        return self._ln == other._ln

    def execute(self):
        return_code = subprocess.call(self._ln, shell=True)
        return return_code

class InitCommand(Command):
    """ A Command object that is only designed to be run once in a list of commands
    """
    def __init__(self, command_line, name=""):
        super(InitCommand, self).__init__(command_line, name)

class Procedure(object):
    """ A procedure composed of multiple commands
        e.g. my_procedure = Procedure( "my_procedure" )
             my_procedure.add(Command("echo Hello"));
             my_procedure.add(InitCommand("echo initCommands always come first"));
             my_procedure.add(Command("echo World"));
             assert len(my_procedure) == 3
             print("executing procedure: " + my_procedure.name())
             for cmd in my_procedure:
                print(" - executing command: " + str(cmd))
                cmd.execute()
    """
    def __init__(self, name="", commands=None):
        self._name = name
        self._init_pos = 0
        self._cmds = []
        if not commands is None:
            self.add(commands)

    def name(self):
        return self._name

    def add(self, command_list):
        """ add a list of commands
            InitCommands will be scheduled to run before any others
            and duplicate InitCommands will be removed
        """
        if not isinstance(command_list, list):
            local_command_list = [ command_list ]
        else:
            local_command_list = command_list
            
        for cmd in local_command_list:
            if isinstance(cmd , Procedure):
                self.add(cmd._cmds)
                continue
            if isinstance(cmd , InitCommand):
                found = False
                #for init_cmd in self._init_cmds:
                for i in range(0 , self._init_pos):
                    if self._cmds[i] == cmd:
                        found = True;
                        break
                if not found:
                    self._cmds.insert(self._init_pos, cmd)
                    self._init_pos += 1
            else:
                self._cmds.append(cmd)

    def commands(self):
        """ return a list of ordered command objects """
        #cmds = self._init_cmds
        #cmds.extend(self._cmds)
        #return cmds
        return self._cmds

    def __iter__(self):
        return self.commands().__iter__()

    def __len__(self):
        return len(self._cmds)

