class Version:
    """
    Class to specify the version numbering schemes and define their order
    """
    def __init__(self, version=""):
        self._version=version

    def matches(self, version):
        return self._version == version._version or version._version == "" or self._version == ""

    def version(self):
        return self._version;

    def __eq__(self, other):
        if not isinstance(self, Version):
            raise(TypeError("expecting a Version"))
        if not isinstance(other, Version):
            raise(TypeError("expecting a Version"))
        return self._version == other._version

    def __str__(self):
        return self._version

