from .DependencyFile import DependencyFile
from .DependencyNode import DependencyNode
from .SpinSpec import SpinSpec

class Spin(object):
    def __init__(self, dependencies):
        self._deps = dependencies
        self._activated = []
        self._activated_map = {}

    def spin_spec(self):
        return SpinSpec(self.name(","))

    def name(self, seperator=","):
        if(len(self._activated) == 0):
            return "default"
        it = iter(self._activated)
        name=next(it).name()
        for dep in it:
            name = name + seperator + dep.name()
        return name

    def fork(self, dependency):
        new_spin = Spin( self._deps )
        new_spin._activated = self._activated[:]
        new_spin._activated_map = self._activated_map.copy()
        new_spin._activated.append(dependency)
        new_spin._activated_map[dependency.name()] = len(new_spin._activated) - 1
        return new_spin

    def private_dependencies(self, deps):
        required = []
        for dep in deps:
            if dep.optional():
                if dep.name() not in self._activated_map:
                    continue
            required.append(dep)
            required.extend(self.private_dependencies(dep.subdependencies()))
        return required

    """All dependencies associated with this spin as a list of Dependency objects
       You can provide an optional dictionary of filters on the dependencies
    """
    def dependencies(self, filters = {}):
        deps = self.private_dependencies(deps=self._deps._root.subdependencies())
        if not filters:
            return deps
        filtered_deps = []
        for dep in deps:
            if dep.matches_attributes(filters):
                filtered_deps.append(dep)
        return filtered_deps

    """ return True if the Spin is compatible with the SpinSpec
    """
    def matches(self, spin_spec):
        if not isinstance(spin_spec, SpinSpec):
            raise(TypeError("expecting a SpinSpec object"))

        deps = self.dependencies()
        if len(deps) < len(spin_spec.dependencies()):
            return False
        if len(deps) == 0:
            return True

        matched = 0
        for dep in deps:
            if not dep.optional():
                continue
            if not spin_spec.matches(dep):
                return False
            matched += 1
        if matched == len(spin_spec):
            return True
        return False

    def __str__(self):
        return self.name()


class Dependencies(object):
    """Process Dependency tree information given a root node"""
    def __init__(self, dependencies_node):
        if isinstance(dependencies_node, str):
            # must be a filename - try and read it in
            #print("reading dependencies file: " + dependencies_node)
            self._root = DependencyFile(dependencies_node).root_node()
        else:
            if isinstance(dependencies_node, DependencyNode):
                self._root = dependencies_node
            else:
                raise(TypeError("Expecting a DependecyNode type or a filename"))

        assert isinstance(self._root, DependencyNode)
        self._spins = []

    def private_generate_spins(self, deps, spin_index):
        """ private method - do not use directly """
        for dep in deps:
            if dep.optional():
                index_limit = len(self._spins)
                for index in range(spin_index, index_limit):
                    self._spins.append( self._spins[index].fork(dep) )
                self.private_generate_spins(dep.subdependencies(), index_limit )

            else:
                self.private_generate_spins(dep.subdependencies(), spin_index)

    def spins(self):
        """ return a list of all possible spins that can be generated """
        if len(self._spins) == 0:
            self._spins = [ Spin(self) ]
            assert isinstance(self._root, DependencyNode)
            self.private_generate_spins(self._root.subdependencies(), 0)
        return self._spins

    def find_spins(self, spin_spec):
        """ search for spins that match a spin specification """
        if not isinstance(spin_spec, SpinSpec):
            raise(TypeError("expecting a SpinSpec object"))
        spins=[]

        # remove non-optional deps from spin spec
        non_optional_deps = self._root.find_with_attribute_recursive( { "optional" : False } )
        to_remove = []
        for dep in spin_spec.dependencies():
            for non_opt in non_optional_deps:
                if non_opt.matches(dep):
                    to_remove.append(dep)
                    break
        for dep in to_remove:
            spin_spec.remove(dep)

        # find matching
        for spin in self.spins():
            if spin.matches(spin_spec):
                spins.append(spin)

        return spins

    def all(self):
        """ return a list of all required dependency packages (excluding build packages)"""
        return self._root.find_with_attribute_recursive( { "build" : False } )

    def build(self, spin = None):
        """ returns the packages containing the build tools required  (i.e. those with a build attribute) """
        return self._root.find_with_attribute_recursive( { "build" : True } )

