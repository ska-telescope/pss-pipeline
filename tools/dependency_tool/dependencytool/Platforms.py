import platform as system_platform
from dependencytool.Platform import Platform

# -----------------------------------------
# import platform and package descriptions
# -----------------------------------------
from dependencytool.platforms.AlpineLinux import AlpineLinux
from dependencytool.platforms.Debian import *
from dependencytool.platforms.Ubuntu import *
from dependencytool.platforms.CentOs import *

class Platforms(object):
    """ Manager of known Platform objects
    """
    def __init__(self, platforms = Platform.subclasses(Platform)):
        self._platforms = {}
        for pl in platforms:
            self._platforms[pl.__name__.lower()] = pl()

    def platform_names(self):
        return self._platforms.keys()

    # get a platform by name
    def platform(self, name):
        return self._platforms[name]

    def platforms(self):
        return self._platforms.values()

    def platform_dictionary(self):
        return self._platforms

    def local(self):
        system = system_platform.system().lower()
        if system == "linux":
            dist = system_platform.dist()
            dist_trial_names = [dist[0].lower() + dist[1]
                              , dist[0].lower() + dist[1].replace(".", "_")
                              , dist[0].lower()]
            for name in dist_trial_names:
                plat = self._platforms.get(name)
                if plat != None:
                    return plat
        if system == "darwin":
            version = system_platform.mac_ver()[0].strip()
            print("MacOSX " + version + " detected. Not yet supported")
        if system == "windows":
            print("Windows detected. Not yet supported")

        return None

