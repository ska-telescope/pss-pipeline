import copy
from .Version import Version

class Dependency(object):
    """Information about the name, version, and other attributes fo a single dependency"""
    def __init__(self, name, version = Version(), optional=False, build=False):
        self._name = name.lower()
        if isinstance(version, Version):
            self._version = version
        else:
            self._version = Version(version)
        self._attr = { "build" : build
                     , "optional": optional
                     }

    def copy(self, other):
        """ copy data from another Dependency instance """
        if not isinstance(other, Dependency):
            raise(TypeError("Expecting a Dependency instance"))
        self._name = other._name
        self._version = other._version
        self._attr = copy.deepcopy(other._attr)

    def name(self):
        return self._name

    def version(self):
        return self._version

    def optional(self):
        """ returns True if the optional attribute has been set """
        return self._attr["optional"]

    def matches(self, dependency):
        """ returns True when the dependency passed is compatible 
            i.e. the same name and version specs that are compatble
        """
        if not isinstance(dependency, Dependency):
            raise(TypeError("Expecting a Dependency instance"))
        if dependency.name() != self._name:
            return False
        if dependency.version() == Version():
            return True
        if self._version == Version():
            return True
        return self._version == dependency.version()

    def matches_attributes(self, filters):
        """ returns False if the attributes specified in the fliter
            does not match those set in this object.
            returns True otherwise.
            e.g. dep.match_attributes( { "build": False, "optional": True } )
        """
        for key in filters:
            attr = self._attr.get(key)
            if attr is None:
                if filters[key] == True:
                    return False
            else:
                if filters[key] != attr:
                    return False
        return True

    def attributes(self):
        return self._attr

