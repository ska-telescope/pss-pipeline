from .Dependency import Dependency

class SpinSpec(object):
    """ String representation to specify a spin
    """
    def __init__(self, string=""):
        self._options = []
        self._str = string
        if len(string) != 0:
            # -- process a string
            line = string.lstrip()
            if line and line != "default":
                self._str = ""
                tokens = line.split(",")
                tokens.sort()
                for token in tokens:
                    self._str = self._str + token + ","
                    self._options.append(Dependency(token.lstrip()))
                self._str = self._str[:-1] # strip off last ","

    def add_dependency(self, dependency):
        """
            add a Dependency to this spin specification
        """
        assert isinstance(dependency, Dependency)
        if self.matches(dependency):
            return
        name = dependency.name()
        tokens = self._str.split(",")
        tokens.append(name)
        tokens.sort()
        strg = ",".join(tokens)
        self._str = strg
        self._options.append(dependency)

    def dependencies(self):
        return self._options

    def remove(self, dependency):
        """
            remove the dependency from this SpinSpec instance
            returns True is the dep was successfully removed False otherwise
        """
        index = 0
        for opt in self._options:
            if dependency.matches(opt):
                del self._options[index]
                return True
            index = index + 1
        return False

    def matches(self, dependency):
        """ return True when the dependency passed is part of this specification
        """
        if not isinstance(dependency, Dependency):
            raise(TypeError("expecting a dependency"))

        for opt in self._options:
            if dependency.matches(opt):
                return True

        return False

    def contains(self, other):
        """
            return True if the dependecies of the spin spec passed are the same as, or are a subset
            of, those in this spin spec
        """
        if not isinstance(dependency, SpinSpec):
            raise(TypeError("expecting a SpinSpec"))
        if len(self._options) < len(other._options):
            return False
        for dep in other._options:
            if not self.matches(dep):
                return False
        return True

    def add(self, other):
        """
            Merge a spin spec into this object, removing any duplicates
        """
        assert isinstance(other, SpinSpec)
        for dep in other._options:
            self.add_dependency(dep)
        return self

    def __eq__(self, other):
        if other is None:
            # equate None with SpinSpec("")
            return self._str == ""
        if len(self._options) != len(other._options):
            return False
        for i in range(len(self._options)):
            if self._options[i] != other._options[i]:
                return False
        return self._str == other._str

    def __iadd__(self, other):
        self.add(other)

    def __add__(self, other):
        result = SpinSpec(self._str)
        return result.add(other)

    def __str__(self):
        return self._str

    def __hash__(self):
        return hash(self._str)

    def __len__(self):
        return len(self._options)
