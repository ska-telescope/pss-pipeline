from dependencytool.Platform import Platform
from dependencytool.PackageManager import ToolConfig, RepoPackage
from .package_managers.AptPackageManager import AptPackageManager
from .package_managers.DebianPackageManager import DebianPackageManager, DebianCudaPackageManager
from .package_managers.CudaRepository import CudaRepository

class Debian(Platform):
    def __init__(self, version="latest", *args):
        super(Debian, self).__init__("Debian" + str(version), DebianPackageManager(),
                ToolConfig("docker", { "image": "debian:" + str(version) + "-slim" } ), *args)

class Debian10(Debian):
    def __init__(self):
        super(Debian10, self).__init__("buster", AptPackageManager()
                                               , CudaRepository(DebianCudaPackageManager)
                                               )

