from dependencytool.Platform import Platform
from dependencytool.Commands import InitCommand
from dependencytool.PackageManager import ToolConfig, RepoPackage
from dependencytool.Package import Package, BuildPackage
from dependencytool.Version import Version
from .package_managers.RedhatPackageManager import RedhatPackageManager, RedhatCudaPackageManager
from .package_managers.YumPackageManager import YumPackageManager
from .package_managers.CudaRepository import CudaRepository

class CentOs(Platform):
    def __init__(self, version="latest", extra_packages={}, *yum_args):
        super(CentOs, self).__init__("CentOs" + str(version)
                    , RedhatPackageManager(extra_packages)
                    , ToolConfig("docker", { "image": "centos:" + str(version) } )
                    , *yum_args)

class CentOs7(CentOs):
    def __init__(self):
        installer = [ InitCommand("yum -y install epel-release") ]
        super(CentOs7, self).__init__(7
                                     , { "cmake" : Package("cmake", Version("2"), attributes = { "cmake": "/usr/bin/cmake" } ) }
                                     , YumPackageManager( packages={ "cmake": Package("cmake3", Version("3"), { "cmake": "/usr/bin/cmake3" }) }, install=installer, name="EPEL" )
                                     , CudaRepository(repo_factory=RedhatCudaPackageManager)
                                     )

