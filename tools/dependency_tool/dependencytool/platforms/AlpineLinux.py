from dependencytool.PackageManager import *
from dependencytool.Platform import Platform
from .package_managers.AlpinePackageManager import AlpinePackageManager

# -- Alpine Linux --
class AlpineLinux(Platform):
    def __init__(self, version="stable", *args):
        super(AlpineLinux, self).__init__("AlpineLinux" + str(version), AlpinePackageManager(),
                ToolConfig("docker", { "image": "docker:" + str(version) } ), *args)

