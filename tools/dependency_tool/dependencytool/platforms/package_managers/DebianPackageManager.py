
from .AptPackageManager import AptPackageManager
from dependencytool.Package import Package, BuildPackage
from dependencytool.Version import Version
from dependencytool.Commands import Command, InitCommand

class DebianPackageManager(AptPackageManager):
    """ defines the debian packages available in the standard debian distros
    """
    def __init__(self, other_packages={}, install=[], name="Debian"):
        packages = {
            # alphabetical order please
            "boost" : Package("libboost-dev")
           ,"boost_filesystem" : Package("libboost-filesystem-dev")
           ,"boost_program_options" : Package("libboost-program-options-dev")
           ,"boost_system" : Package("libboost-system-dev")
           ,"cmake" : Package("cmake")
           ,"fftw" : Package("libfftw3-dev")
           ,"g++" : Package("g++")
           ,"gcc" : Package("gcc")
           ,"make" : Package("make")
           ,"nasm" : Package("nasm", attributes = { "nasm": "/usr/bin/nasm" } )
           ,"python-pip": Package("python3-pip")
           ,"python-venv": Package("python3-venv")
           ,"python": Package("python3")
        }
        packages.update(other_packages)
        super(DebianPackageManager, self).__init__(packages, install, name)

class DebianCudaPackageManager(AptPackageManager):
    def __init__(self, cuda_version):
        packages = {
           "cuda" : Package("cuda-{}".format(cuda_version))
           ,"cuda-runtime" : Package("cuda-runtime-{}".format(cuda_version))
           ,"cuda-toolkit" : BuildPackage("cuda-toolkit-{}".format(cuda_version))
           ,"cuda-cufft" : [ Package("cuda-cufft-{}".format(cuda_version)) , BuildPackage("cuda-cufft-dev-{}".format(cuda_version)) ]
           ,"cuda-curand" : [ Package("cuda-curand-{}".format(cuda_version)) , BuildPackage("cuda-curand-dev-{}".format(cuda_version)) ]
        }
        cuda_repo_base = "https://developer.download.nvidia.com/compute/cuda/repos/debian10/x86_64"
        installer = [ InitCommand('apt-get install -y software-properties-common'),
                      InitCommand('apt-get install -y gnupg'),
                      InitCommand('apt-get install -y man'),
                      InitCommand('mkdir /usr/share/man/man1/'),
                      InitCommand('add-apt-repository "deb ' + cuda_repo_base + '/ /"'),
                      InitCommand('apt-key adv --fetch-keys ' + cuda_repo_base + '/7fa2af80.pub'),
                      InitCommand('add-apt-repository contrib'),
                      InitCommand('export DEBIAN_FRONTEND=noninteractive'),
                      InitCommand('apt-get update -y'),
                    ]
        super(DebianCudaPackageManager, self).__init__(packages, install=installer, name="CudaDebian")

