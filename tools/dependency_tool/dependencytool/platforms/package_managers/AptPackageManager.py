from dependencytool.PackageManager import RepoPackage, PackageManager
from dependencytool.Commands import Command, InitCommand

class AptPackageManager(PackageManager):
    def __init__(self, packages={}, install=[], name="Apt"):
        super(AptPackageManager, self).__init__(name, packages, install)

    def update_commands(self):
        return [ InitCommand("apt-get update") ]

    def install_commands(self, packages):
        repo_packages = []
        for pkg in packages:
            repo_packages.append(pkg.name())

        cmds = []
        if repo_packages:
            cmds.append(Command("apt-get install -y " + (" ".join(repo_packages))));
        return cmds
