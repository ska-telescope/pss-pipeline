from copy import deepcopy
from dependencytool.PackageManager import PackageManager
from dependencytool.Package import Package, BuildPackage
from dependencytool.Commands import Command, InitCommand

class CudaRepository(PackageManager):
    """ top level management of Cuda dependecies
        This module will match cuda deps and map them to the apporopriate
        platform and version specific repoistory that matches the dependency
        versioning requiremnts
        Pass the platform specific repository type to the constructor. This constructor
        must take the cuda version
    """
    def __init__(self, repo_factory):
        self._repo_factory=repo_factory
        packages = {
                "cuda" : [ Package("cuda-runtime"), BuildPackage("cuda-toolkit") ]
               ,"cuda-cufft" : Package("cuda-cufft")
               ,"cuda-curand" : Package("cuda-curand")
        }
        super(CudaRepository, self).__init__("nvidia_cuda_repo", packages)

    def package(self, dependency, spin):
        pkg = super(CudaRepository, self).package(dependency, spin)
        if pkg == Package():
            return pkg
        if(str(dependency.version()) == ""):
            raise RuntimeError("cuda dependencies require an explicit version")

        # -- determine the repo
        repo = self._repo_factory( dependency.version() );
        if dependency.name() != pkg.name():
            dependency._name = pkg.name()
            dependency_copy = deepcopy(dependency)
            pkg = repo.package(dependency_copy, spin)
            if pkg is not Package():
                return pkg
        return repo.package(dependency, spin);

