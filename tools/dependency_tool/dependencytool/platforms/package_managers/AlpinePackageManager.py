from dependencytool.PackageManager import RepoPackage, PackageManager
from dependencytool.Package import Package
from dependencytool.Version import Version
from dependencytool.Commands import *

class AlpinePackageManager(PackageManager):
    def __init__(self, packages={
                            "python" : Package("python3", Version("3"), attributes = { "python": "/usr/bin/python3" })
                           ,"gzip" : Package("gzip", attributes = { "gzip": "/usr/bin/gzip" })
                       }
               , install=[], name="Apk"):
        super(AlpinePackageManager, self).__init__(name, packages, install)

    def update_commands(self):
        return [ InitCommand("apk update") ]

    def install_commands(self, packages):
        repo_packages = []
        for pkg in packages:
            repo_packages.append(pkg.name())

        cmds = []
        if len(repo_packages) != 0:
            cmds.append(Command("apk add " + (" ".join(repo_packages))));
        return cmds

