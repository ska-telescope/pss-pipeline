from dependencytool.PackageManager import RepoPackage, PackageManager
from dependencytool.Commands import Command, InitCommand

class YumPackageManager(PackageManager):
    def __init__(self, packages={}, install=[], name="Yum"):
        super(YumPackageManager, self).__init__(name, packages, install)

    def update_commands(self):
        return [ InitCommand("yum -y update") ]

    def install_commands(self, packages):
        repo_packages = []
        for pkg in packages:
            repo_packages.append(pkg.name())

        cmds = []
        if repo_packages:
            cmd = "yum install -y " + (" ".join(repo_packages))
            cmds.append(Command(cmd));
        return cmds

