""" Unit tests for the Spin class"""
from dependencytool.DependencyNode import DependencyNode
from dependencytool.Dependencies import Dependencies
from dependencytool.Dependency import Dependency
from dependencytool.SpinSpec import SpinSpec

def test_spins_no_deps():
    root_node = DependencyNode(Dependency("Root"))
    deps = Dependencies(root_node)
    spins = deps.spins()
    assert len(spins) == 1
    assert spins[0].name() == "default"

    """ access via find """
    spin_spec = SpinSpec("default");
    found_spins = deps.find_spins(spin_spec);
    assert len(found_spins) == 1

def test_spins_no_optionals():
    root_node = DependencyNode(Dependency("Root"))
    root_node.add_dependency(Dependency("a"))
    deps = Dependencies(root_node)
    spins = deps.spins()
    assert len(spins) == 1
    assert spins[0].name() == "default"
    assert len(spins[0].dependencies()) == 1

def test_spins_no_optionals_multi_deps():
    root_node = DependencyNode(Dependency("Root"))
    root_node.add_dependency(Dependency("a"))
    root_node.add_dependency(Dependency("b"))
    root_node.add_dependency(Dependency("c"))
    deps = Dependencies(root_node)
    spins = deps.spins()
    assert len(spins) == 1
    assert spins[0].name() == "default"
    assert len(spins[0].dependencies()) == 3

def test_spins_single_optional():
    root_node = DependencyNode(Dependency("Root"))
    root_node.add_dependency(Dependency("a", optional=True ))
    deps = Dependencies(root_node)
    spins = deps.spins()
    assert len(spins) == 2
    assert spins[0].name() == "default"
    assert len(spins[0].dependencies()) == 0
    assert spins[1].name() == "a"
    assert len(spins[1].dependencies()) == 1

    """ access via find """
    spin_spec = SpinSpec("default");
    found_spins = deps.find_spins(spin_spec);
    assert len(found_spins) == 1
    assert found_spins[0].name() == "default"

    found_spins = deps.find_spins(SpinSpec("a"));
    assert len(found_spins) == 1
    assert found_spins[0].name() == "a"

def test_spins_single_optional_top_optional_subdeps():
    root_node = DependencyNode(Dependency("Root"))
    root_node.add_dependency(Dependency("a", optional=True ))
    root_node.subdependencies()[0].add_dependency(Dependency("b", optional=True ))
    root_node.subdependencies()[0].add_dependency(Dependency("c"))
    deps = Dependencies(root_node)
    spins = deps.spins()
    assert len(spins) == 3
    assert spins[0].name() == "default"
    assert spins[1].name() == "a"
    assert spins[2].name() == "a,b"

def test_spins_multiple_optional_same_layer():
    root_node = DependencyNode(Dependency("Root"))
    root_node.add_dependency(Dependency("a", optional=True ))
    root_node.add_dependency(Dependency("b", optional=True ))
    deps = Dependencies(root_node)
    spins = deps.spins()
    assert len(spins) == 4
    assert spins[0].name() == "default"
    assert spins[1].name() == "a"
    assert spins[2].name() == "b"
    assert spins[3].name() == "a,b"
