from dependencytool.Commands import Procedure
from dependencytool.Commands import Command

class DockerRun(object):
    def __init__(self, docker, options=None):
        self._docker=docker
        self._options=options

    def options(self):
        return self._optons

    def command_line(self, cmds):
        """ generate a string to execute the command procedure provided using docker run
        """
        if not isinstance(cmds, Procedure):
            cmds = Procedure("docker run", commands=cmds)
        if len(cmds) == 0:
            return None

        exe = str(self)
        # TODO extract platform specific shell from docker object
        shell = "/bin/sh -c"
        exe += " " + shell + " \""

        it=iter(cmds)
        exe += str(next(it))
        for cmd in it:
            exe += " && " + str(cmd)
        exe += "\""
        return exe

    def command(self, cmds):
        return Command(self.command_line(cmds))

    def __str__(self):
        exe = self._docker.docker_exe() + " run"
        if not self._options is None:
            exe += " " + self._options
        return exe
