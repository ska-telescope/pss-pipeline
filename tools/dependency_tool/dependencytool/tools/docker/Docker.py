# ----------------------
# Docker Tool
# ----------------------
class Docker:
    def __init__(self, platform, params = {} ):
        self._params = params;
        self._platform = platform

    def image(self):
        return self._params.get("image")

    def docker_exe(self):
        docker = self._platform.executable("docker")
        return docker
