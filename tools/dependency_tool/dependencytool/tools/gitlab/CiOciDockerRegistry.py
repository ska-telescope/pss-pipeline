from .CiStage import CiStage
from dependencytool.platforms.AlpineLinux import AlpineLinux
from dependencytool.Dependency import Dependency
from dependencytool.Commands import Command, InitCommand, Procedure

class CiOciDockerRegistry (object):
    """ An OciImageStrategy that share container images between stages
        via the gitlab docker registry
    """
    def __init__(self, platform, build_config, project, app_name):
        self._docker_tool = project.tool("Docker", platform)
        self._build_config = build_config
        self._name = self._build_config.name() + "_" + platform.name()
        self._platform = platform

        # TODO make configurable
        ci_docker_platform = AlpineLinux()

        ci_docker_tool = project.tool("Docker", ci_docker_platform)
        self._docker_tool = ci_docker_tool
        self._docker_exe = ci_docker_tool.docker_exe()
        python_exe = ci_docker_platform.executable("python", Dependency("python"))
        self._app_exe = python_exe + " " + app_name
        self._ci_packages = ci_docker_platform.install_commands(
                                   [ Dependency("python") ])

        self._image_tag = "${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}/" + self._name.lower() + ":$CI_COMMIT_REF_SLUG" + "-" + "$CI_COMMIT_SHORT_SHA"
        self._docker_login = Procedure("load_docker_image",
                                        [
                                            InitCommand("echo $CI_REGISTRY_PASSWORD | " + self._docker_exe + " login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY")
                                        ] 
                                       )

    def stage_configure(self):
        container_stage = self._name + "_build_container"
        spin_name = self._build_config.spin().name()
        stage = CiStage( name=container_stage
                       , stage="configure"
                       , image=self._docker_tool.image()
                       , services=[ "docker:dind" ]
                       , before_script = Procedure( commands=[ self._ci_packages ])
                       , procedure = Procedure( commands= [ Command(self._app_exe
                               + " -p " + self._platform.id()
                               + " -s " + spin_name
                               + " dockerfile -b"
                               + " | "
                               + self._docker_exe
                               + " build"
                               + " -t " + self._image_tag 
                               + " -")
                           , self._docker_login
                           , Command(self._docker_exe
                               + " push " + self._image_tag
                               )
                           ]
                         )
                       )
        return [ stage ]

    def before_script(self):
        return None

    def stage(self, stage, name, procedure, artifacts, work_dir):
        ci_stage = CiStage( name=name
                          , stage=stage
                          , image=self._image_tag
                          , before_script=self.before_script()
                          , procedure=procedure
                          , artifacts=artifacts
                          )
        return ci_stage
