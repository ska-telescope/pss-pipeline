from __future__ import print_function

class CiStage(object):
    def __init__(self, name, stage
               , procedure=None
               , before_script=None
               , artifacts=None
               , services=None
               , cache=None
               , tags=None
               , needs=None
               , image=None):
        self._name = name
        self._image = image
        self._stage = stage
        if not tags is None:
            if isinstance(tags, list):
                self._tags = tags
            else:
                self._tags = [ tags ]
        else:
            self._tags = tags
        self._procedure = procedure
        self._before_script = before_script
        self._cache = cache
        self._artifacts = artifacts
        if services is None:
            self._services = []
        else:
            if isinstance(services, list):
                self._services = services
            else:
                self._services = [ services ]
        if needs is None:
            self._needs = []
        else:
            if isinstance(needs, list):
                self._needs = needs
            else:
                self._needs = [ needs ]
        self._formatting = { "indent" : "  " }

    def name(self):
        return self._name

    def stage(self):
        return self._stage

    def add_needs(self, needs):
        if not isinstance(needs, list):
            raise TypeError("expecting a list of nneds but got " + str(type(needs)))
        for need in needs:
            self._needs.append(need)

    def write(self):
        print (self._name + ":" )
        indent = self._formatting["indent"]
        print ( indent + "stage: " + self._stage  )

        # -- tags
        if not self._tags is None and len(self._tags) > 0:
            print ( indent + "tags:")
            for tag in self._tags:
                print ( indent + indent + "- " + tag )

        # -- image
        if self._image is not None:
            print(indent + "image: " + self._image)

        # -- services
        if len(self._services) > 0:
            print ( indent + "services:")
            for tag in self._services:
                print ( indent + indent + "- " + tag )

        # -- before script
        if not self._before_script is None:
            print("  before_script:")
            for cmd in self._before_script:
                print ( indent + indent + "- " + str(cmd) )

        # -- script
        if not self._procedure is None and len(self._procedure) > 0:
            print(indent + "script:")
            for cmd in self._procedure:
                print ( indent + indent + "- " + str(cmd) )

        # -- cache
        if not self._cache is None:
            self._cache.write( formatting=self._formatting )

        # -- artifacts
        if not self._artifacts is None:
            self._artifacts.write( formatting=self._formatting )

        # -- needs
        if len(self._needs) > 0:
            print ( indent + "needs:")
            for tag in self._needs:
                print ( indent + indent + "- " + tag )

