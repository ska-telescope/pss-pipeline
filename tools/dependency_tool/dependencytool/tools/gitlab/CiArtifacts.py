class CiArtifacts(object):
    def __init__(self, paths=[]):
        self._paths = paths

    def write(self, formatting={ "indent" : "  " }):
        indent = formatting["indent"]
        print (indent + "artifacts:");
        indent += indent
        if not self._paths is None and len(self._paths) > 0:
            print (indent + "paths:");
            for path in self._paths:
                print (indent + indent + "- " + path);

