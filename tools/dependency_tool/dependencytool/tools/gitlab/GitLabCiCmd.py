from .CiStage import CiStage
from .CiCache import CiCache
from .CiArtifacts import CiArtifacts
from .CiConfig import CiConfig
from dependencytool.Commands import Procedure

class GitLabCiCmd():
    """ User command handler to generate gitlab_ci.yml files for testing all build combos """
    def help(self):
        return "generate gitlab's CI description in yml format (to stdout)"

    def options(self, subparser):
        subparser.add_argument("-r", "--required", action='store_true', help="minimalist with required components only")

    def fn(self, args):
        print ("# file generated with " + args.executable_name())
        indent = "    "

        ci_config = args.project().tool_config("gitlab_ci")
        project = args.project()

        print ("stages:")
        stages = ["configure"]
        for platform in project.platforms():
            for build_config in project.configurations():
                platform_builder = project.builder(build_config, platform)
                for build_stage in platform_builder.stages():
                    if not build_stage.name() in stages:
                        stages.append(build_stage.name())

        for stage in stages:
            print ("  - " + stage)
        print("")

        tool_name = "tools/" + args.executable_name() + args.dependencies_filenames_option()

        for platform in project.platforms():
            print ("# -----------------------------------------")
            print ("# Targets for Platform " + platform.name())
            print ("# -----------------------------------------")
            for build_config in project.configurations():
                platform_builder = project.builder(build_config, platform)
                name = build_config.name() + "_" + platform.name()

                oci_image_strategy = ci_config.oci_image_strategy(platform, build_config, project, tool_name)
                assert not oci_image_strategy is None
                # -- container configuration stage --
                needs=[]
                for stage in oci_image_strategy.stage_configure():
                    needs.append(stage.name())
                    stage.write()

                #spin_name = build_config.spin().name()
                print("")

                work_dir = "/home"
                artifacts = CiArtifacts( paths = [ platform_builder.build_directory() ])

                for build_stage in platform_builder.stages():
                    build_stage_name = name + "_" + build_stage.name()
                    build_procedure = Procedure(name=build_stage_name)
                    for cmd in build_stage:
                        build_procedure.add(cmd)
                    stage = oci_image_strategy.stage( stage=build_stage.name()
                                                    , name=build_stage_name
                                                    , procedure=build_procedure
                                                    , artifacts=artifacts
                                                    , work_dir=work_dir)
                    stage.add_needs(needs)
                    needs.append(build_stage_name)
                    stage.write()
                    print("")

        return 0
