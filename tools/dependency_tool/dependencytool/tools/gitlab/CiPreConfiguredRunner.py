from .CiStage import CiStage

class CiPreConfiguredRunner(object):
    """ An OciImageStrategy that share container images between stages
        via the gitlab docker registry
    """
    def __init__(self, platform, build_config, tagger=None):
        self._build_config = build_config
        # create tags
        self._tagger = tagger
        self._platform = platform

    def tags(self, stage):
        """
            return the list of tags for the given stage
        """
        # TODO dependencies should be dependent on the stage
        tags = []
        for dep in self._build_config.dependencies():
            tag = self._tagger.tag(dep)
            if tag:
                tags.append(tag)
        return tags

    def stage_configure(self):
        """
            return the list of steps for the configure stage
            Required method for CI policy objects
        """
        return []

    def stage(self, stage, name, procedure, artifacts, work_dir):
        """
            required interface for all CI policies
        """
        ci_stage = CiStage( name=name
                          , stage=stage
                          , tags=self.tags(stage)
                          , procedure=procedure
                          , artifacts=artifacts
                          )
        return ci_stage
