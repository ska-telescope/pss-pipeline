from .CiOciCached import CiOciCached
from .CiPreConfiguredRunner import CiPreConfiguredRunner
from .CiOciUnshared import CiOciUnshared
from .CiOciDockerRegistry import CiOciDockerRegistry
from dependencytool.Version import Version
from dependencytool.SpinSpec import SpinSpec

class TagDictionary(object):
    """
        A Ci Tagger class that uses a simple dictionary lookup
    """
    def __init__(self, tag_dict):
        self._tags = tag_dict

    def tag(self, dependency):
        return self._tags.get(dependency.name())

class TagDependency(object):
    """
        A Ci Tagger class that generates tags
        based on the depdendency names
    """
    def __init__(self, tag_dict):
        self._tags = tag_dict

    def tag(self, dep):
        """
            return the gitlab tag associated with the dependecy dep
        """
        tag = dep.name()
        if (dep.version() != Version()) or not dep.version() is None:
            tag = tag + "-" + str(dep.version())
        return tag

class CiPreConfiguredRunnerFactory(object):
    """
        A policy factory that creates a CiPreConfiguredRunner policy
        configured with the tagging object provided
        tags can be a simple dictionary mapping dependency names to
        tagnames, or a Tagging object (i.e an object that returns the tag
        when its tag(dependency) method is called.
    """
    def __init__(self, tags):
        if isinstance(tags, dict):
            self._tags = TagDictionary(tags)
        else:
            self._tags = tags

    def create(self, platform, build_config, project, app_name):
        """
            Creates a CiPreConfiguredRunner object.
            This method is required for add CI policy factories
        """
        return CiPreConfiguredRunner(platform, build_config, tagger=self._tags)

class PolicyQueryIterator(object):
    """
        A private class used to iterate through the conditions specified in a PolicyQuery object
        Do not use this class directly. this class will be returned when you call

        iter(policy_object)

        and will act as a normal python iterator
    """
    def __init__(self, query):
        assert isinstance(query, PolicyQuery)
        self._str = [str(query)]
        if query.platform() != "":
            if query.spin_spec() != SpinSpec():
                self._str.append("::" + str(query.spin_spec()))
            self._str.append(query.platform() + "::")
        self._it=iter(self._str)

    def __next__(self):
        return self._it.__next__()

class PolicyQuery(object):
    """
        A class to specify the conditions to associate with a polciy
    """
    def __init__(self, spin_spec=None, platform=None):
        if isinstance(str, SpinSpec):
            spin_spec = SpinSpec(spin_spec)
        self._spin_spec = spin_spec
        if platform is None:
            self._platform = ""
        else:
            if isinstance(platform, str):
                self._platform = platform
            else:
                self._platform = platform.name()
        self._platform = self._platform.lower()

    def platform(self):
        return self._platform

    def spin_spec(self):
        return self._spin_spec

    def __iter__(self):
        return PolicyQueryIterator(self)

    def __str__(self):
        return self._platform + "::" + str(self._spin_spec)


class CiConfig(object):
    """
        This class encapsulates the allocation of different CiPolicies (e.g. runners, dockerfile generation, etc)
        to different spin and platform combinations
    """
    def __init__(self, policies=None):
        """
            constructor of th CiConfig object. Configuration to inform the gitlab tool CI pipeline generation
            optional params:
                policies: Allows the user to associate specific policies with a platform and a set of spins.
                          This is a convenience option that generates calls to set_policy.
                          It should defined a dictionary mapping platform names to dictionaries of spins mapped to policy factories
                          For each spin in the map, set_policy calls will be made not only for the spins themselves
                          but also combinations of them where the have the same factory obejet e.g.

                             policies = { "ubuntu" : {  "dep_a" : runner, "dep_b": runner } }

                          (where runner is a suitable policy factory)

                          This option will genererate calls to:

                             set_policy(PolicyQuery("ubuntu", SpinSpec("dep_a")), runner)
                             set_policy(PolicyQuery("ubuntu", SpinSpec("dep_b")), runner)
                             set_policy(PolicyQuery("ubuntu", SpinSpec("dep_a,dep_b")), runner)

        """
        self._config = {}
        if policies:
            for platform_name in policies:
                factories = {}
                spin_map = policies[platform_name]
                for spin in spin_map:
                    factory = spin_map[spin]
                    spin_spec = SpinSpec(spin)
                    if id(factory) not in factories:
                        factories[id(factory)] = []
                    factories[id(factory)].append(spin_spec)
                    self.set_policy(PolicyQuery(platform=platform_name, spin_spec=spin_spec), factory)
                # add policies for all spin combos sharing the same factory
                for factory in factories:
                    spins = factories[factory]
                    self.set_platform_spin_combos(platform_name, factory, spins)

        self._strategies = {}

    def set_platform_spin_combos(self, platform_name, policy_factory, spin_list):
        for s_index in range(0, len(spin_list)):
            spin = spin_list[s_index]
            # FIXME: this does not generate all combos required
            for j in range(s_index+1, len(spin_list) ):
                spin = spin + spin_list[j]
                self.set_policy(PolicyQuery(platform=platform_name, spin_spec=spin), policy_factory)
                self.set_platform_spin_combos(platform_name, policy_factory, spin_list[j:])


    def set_policy(self, policy_query, policy_factory):
        """
            Associate a policy factory (a functor that creates a policy object on demand) to be called
            whenever the provided policy_query condition is met.
            The factory must support the create((platform, build_config, project, app_name)) method and return
            a suitable CiPolicy class configured as required.
        """
        assert isinstance(policy_query, PolicyQuery)
        self._config[str(policy_query)] = policy_factory

    def add_strategy(self, query, strategy):
        """
            Add a strategy object to be associated with the provided query
        """
        self._strategies[str(query)] = strategy
        return strategy

    def _find_ci_strategy_factory(self, query):
        for key in query:
            cfg = self._config.get(key)
            if cfg:
                return cfg
        return None

    def _find_ci_strategy(self, query):
        for key in query:
            st = self._strategies.get(key);
            if st:
                return st
        return None

    def oci_image_strategy(self, platform, build_config, project, app_name):
        """ return the strategy for transfering oci container images
            between jobs in the ci pipeline
        """
        query = PolicyQuery(platform=platform.name(), spin_spec=SpinSpec(build_config.spin().name(",")))
        # if we have an object already available then use it
        strategy = self._find_ci_strategy(query)
        if strategy:
            return strategy

        # is there a custom description from the config
        factory = self._find_ci_strategy_factory(query)
        if factory is None:
            # default
            return CiOciDockerRegistry(platform, build_config, project, app_name)

        return self.add_strategy(query, factory.create(platform, build_config, project, app_name))
