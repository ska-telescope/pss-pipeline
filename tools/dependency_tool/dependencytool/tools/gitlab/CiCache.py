class CiCache(object):
    def __init__(self, key, paths=[]):
        self._key = key
        self._paths = paths

    """ returns the configured paths
    """
    def paths(self):
        return self._paths

    def write(self, formatting={ "indent" : "  " }):
        indent = formatting["indent"]
        print (indent + "cache:");
        indent += formatting["indent"]
        print (indent + "key: " + self._key);
        if not self._paths is None and len(self._paths) > 0:
            print (indent + "paths:");
            for path in self._paths:
                print (indent + indent + "- " + path);

