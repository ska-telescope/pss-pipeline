from dependencytool.platforms.package_managers.YumPackageManager import YumPackageManager
from dependencytool.platforms.package_managers.AptPackageManager import AptPackageManager

class CpackCmd():
    """ User command handler to generate CPack tool dep settings"""
    def help(self):
        return "generate cpack variables"

    def options(self, subparser):
        subparser.add_argument("-b", "--build", action='store_true', help="dev components only")

    def fn(self, args):
        platform = args.get_platform()
        if platform is None:
            return 1

        # early check that spins have been defined 
        args.get_spins()

        # -- default use the first package manager for this platform
        if not platform.package_managers():
            raise Exception("No package managers on this platform")
        package_mgr = platform.package_managers()[0]

        pfu = ""
        if isinstance(package_mgr, YumPackageManager):
            pfu = "RPM"
            output_file = ".rpm"
            dep_var = "CPACK_" + pfu + "_PACKAGE_REQUIRES"
        elif isinstance(package_mgr, AptPackageManager):
            pfu = "DEB"
            keyword = "DEPENDS"
            dep_var = "CPACK_DEBIAN_PACKAGE_DEPENDS"
            output_file = ".deb"
        else:
            raise TypeError("CpackCmd: Unsupported package type")

        print("set(CPACK_GENERATOR " + pfu + ")")
        print("set(PACKAGE_FILE \"${CPACK_PACKAGE_NAME}" + output_file + "\")")
    
        # determine and print out the dependency package names
        if args.build():
            version = "build"
            filters = {}
        else:
            version = "runtime"
            filters = { "build" : False  }

        pkgs = platform.packages(args.get_spin_deps(filters), version)


        print("set(" + dep_var)
        for pkg in pkgs:
            print("    " + pkg.name())
        print(")")
        
