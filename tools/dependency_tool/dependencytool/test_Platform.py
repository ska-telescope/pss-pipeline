""" Unit tests for the Platform class"""
from dependencytool.Platform import Platform

def test_name():
    platform = Platform("test_platform")
    assert platform.name() == "test_platform"
    assert platform.id() == "Platform"
