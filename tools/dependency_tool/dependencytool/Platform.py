from .PackageManager import *
from .Dependencies import Dependencies
from .Commands import Procedure

class Platform(object):
    def __init__(self, name, *package_managers):
        self._name = name
        self._package_managers = []
        self._tool_configs = {}
        for mgr in package_managers:
            if isinstance(mgr, PackageManager):
                self._package_managers.append(mgr)
            elif isinstance(mgr, ToolConfig):
                self._tool_configs[mgr.name()] = mgr
        self._executables = {}

    def name(self):
        return self._name

    def id(self):
        return type(self).__name__

    def packages(self, dependency_list, spin = "runtime"):
        pkgs = []
        if isinstance(dependency_list, Dependencies):
            dependency_list = dependency_list.all()

        for dep in dependency_list:
            if isinstance(dep, RepoPackage):
                pkgs.append(dep)
                next
            for mgr in self._package_managers:
                pkg = mgr.package(dep, spin)
                if pkg != Package():
                    pkgs.append(pkg)
        return pkgs;

    def package_managers(self):
        return self._package_managers

    def executable(self, name, dependency = None, spin = ""):
        """ The name of an executable on this specific platform
            Will return the name as is if the executable is not specifically registered
            The dependency information is used for any platform or version specifics
            for the required executable
        """
        if dependency is None:
            return self._executables.get(name, name);

        pkgs = self.packages([dependency], spin);
        if len(pkgs) == 0:
            return self._executables.get(name, name);

        cmd = pkgs[0].attribute(name);
        if cmd is None:
            return self._executables.get(name, name);
        return cmd


    def install_commands(self, package_list, spin = "runtime"):
        """ generate the commands required to install the packages in
            the package_list (unknown packages are ignored)
        """
        _package_list = self.packages(package_list, spin)
        # sort pkg list by package manager
        repos = {}
        for pkg in _package_list:
            repos.setdefault(pkg.repo(),[]).append(pkg)

        cmd_list = Procedure("package_install")
        for repo in repos:
            cmd_list.add(repo.install_repository())
            cmd_list.add(repo.update_commands())
            cmd_list.add(repo.install_commands(repos[repo]))

        return cmd_list

    def tool_config(self, tool_name):
        """Allow extensible parameterisation for platform specific tool config """
        return self._tool_configs.get(tool_name)

    @staticmethod
    def subclasses(cls):
        """ return all known Platform types """
        return cls.__subclasses__()  + [g for s in cls.__subclasses__() for g in s.subclasses(s)]

